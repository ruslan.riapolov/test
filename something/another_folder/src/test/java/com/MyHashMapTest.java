package com;

import com.collection.impl.MyHashMap;
import com.epm.lab.collections.Map;
import com.object.My;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;

/**
 * MyHashMapTest
 *
 * @author Ruslan Riapolov
 * @version 1.0 8/5/2018 10:25 AM
 */
public class MyHashMapTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Map<My, String> myHashMap = new MyHashMap<>();

    @Test
    public void get() {
        //GIVEN
        String expectedResult = "Simple Data";
        //WHEN
        myHashMap.put(new My(expectedResult), expectedResult);
        String actualNullResult = myHashMap.get(new My("Unexisting"));
        String actualResult = myHashMap.get(new My(expectedResult));
        //THEN
        Assert.assertNull(actualNullResult);
        Assert.assertNotNull(actualResult);
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void putNullKey() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Key cannot be null!!!");
        myHashMap.put(null, "NULL key");
    }

    @Test
    public void putNullValue() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Value cannot be null!!!");
        myHashMap.put(new My("NULL value"), null);
    }

    @Test
    public void put() {
        //GIVEN
        String expectedResult0 = "Initial";
        String expectedResult1 = "1111";
        String expectedResult2 = "Changed";
        String expectedResult3 = "33333";
        String expectedResult4 = "444";
        String expectedResult5 = "Once more";
        //WHEN
        myHashMap.put(new My(expectedResult0), expectedResult0);
        myHashMap.put(new My(expectedResult1), expectedResult1);
        myHashMap.put(new My(expectedResult2), "something before replace");
        myHashMap.put(new My(expectedResult3), expectedResult3);
        myHashMap.put(new My(expectedResult2), expectedResult2);
        myHashMap.put(new My(expectedResult4), expectedResult4);
        myHashMap.put(new My(expectedResult5), expectedResult5);
        myHashMap.put(new My("0000000000"), "0000000000");
        myHashMap.put(new My("1111111111"), "1111111111");
        myHashMap.put(new My("2222222222"), "2222222222");
        myHashMap.put(new My("3333333333"), "3333333333");
        myHashMap.put(new My("4444444444"), "4444444444");
        myHashMap.put(new My("5555555555"), "5555555555");
        myHashMap.put(new My("6666666666"), "6666666666");
        myHashMap.put(new My("7777777777"), "7777777777");
        myHashMap.put(new My("8888888888"), "8888888888");
        myHashMap.put(new My("9999999999"), "9999999999");
        String actualNullResult = myHashMap.get(new My("Unexisting"));
        String actualResult0 = myHashMap.get(new My(expectedResult0));
        String actualResult1 = myHashMap.get(new My(expectedResult1));
        String actualResult2 = myHashMap.get(new My(expectedResult2));
        String actualResult3 = myHashMap.get(new My(expectedResult3));
        String actualResult4 = myHashMap.get(new My(expectedResult4));
        String actualResult5 = myHashMap.get(new My(expectedResult5));
        //THEN
        Assert.assertNull(actualNullResult);
        Assert.assertNotNull(actualResult0);
        Assert.assertNotNull(actualResult1);
        Assert.assertNotNull(actualResult2);
        Assert.assertNotNull(actualResult3);
        Assert.assertNotNull(actualResult4);
        Assert.assertNotNull(actualResult5);
        Assert.assertEquals(expectedResult0, actualResult0);
        Assert.assertEquals(expectedResult1, actualResult1);
        Assert.assertEquals(expectedResult2, actualResult2);
        Assert.assertEquals(expectedResult3, actualResult3);
        Assert.assertEquals(expectedResult4, actualResult4);
        Assert.assertEquals(expectedResult5, actualResult5);
    }

    @Test
    public void size() {
        //GIVEN
        int expectedSize0 = 0;
        int expectedSize1 = 6;
        int expectedSize2 = 2;
        int expectedSize3 = 17;
        //WHEN
        int actualSize0 = myHashMap.size();
        myHashMap.put(new My("Initial"), "Initial");
        myHashMap.put(new My("111"), "111");
        myHashMap.put(new My("Changed"), "Changed");
        myHashMap.put(new My("33333"), "33333");
        myHashMap.put(new My("444"), "444");
        myHashMap.put(new My("Once more"), "Once more");
        int actualSize1 = myHashMap.size();
        Iterator<Map.Entry<My, String>> iterator = myHashMap.iterator();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.remove();
        int actualSize2 = myHashMap.size();
        myHashMap.put(new My("0000000000"), "0000000000");
        myHashMap.put(new My("1111111111"), "1111111111");
        myHashMap.put(new My("2222222222"), "2222222222");
        myHashMap.put(new My("3333333333"), "3333333333");
        myHashMap.put(new My("4444444444"), "4444444444");
        myHashMap.put(new My("5555555555"), "5555555555");
        myHashMap.put(new My("6666666666"), "6666666666");
        myHashMap.put(new My("7777777777"), "7777777777");
        myHashMap.put(new My("8888888888"), "8888888888");
        myHashMap.put(new My("9999999999"), "9999999999");
        myHashMap.put(new My("AAAAAAAAAA"), "AAAAAAAAAA");
        myHashMap.put(new My("BBBBBBBBBB"), "BBBBBBBBBB");
        myHashMap.put(new My("CCCCCCCCCC"), "CCCCCCCCCC");
        myHashMap.put(new My("DDDDDDDDDD"), "DDDDDDDDDD");
        myHashMap.put(new My("EEEEEEEEEE"), "EEEEEEEEEE");
        myHashMap.put(new My("FFFFFFFFFF"), "FFFFFFFFFF");
        iterator = myHashMap.iterator();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.remove();
        int actualSize3 = myHashMap.size();
        //THEN
        Assert.assertEquals(expectedSize0, actualSize0);
        Assert.assertEquals(expectedSize1, actualSize1);
        Assert.assertEquals(expectedSize2, actualSize2);
        Assert.assertEquals(expectedSize3, actualSize3);
    }

    @Test
    public void hasNext() {
        //GIVEN
        //WHEN
        myHashMap.put(new My("Initial"), "Initial");
        myHashMap.put(new My("111"), "111");
        myHashMap.put(new My("Changed"), "Changed");
        myHashMap.put(new My("33333"), "33333");
        myHashMap.put(new My("444"), "444");
        myHashMap.put(new My("Once more"), "Once more");
        Iterator<Map.Entry<My, String>> iterator = myHashMap.iterator();
        boolean actual0 = iterator.hasNext();
        iterator.next();
        boolean actual1 = iterator.hasNext();
        iterator.next();
        boolean actual2 = iterator.hasNext();
        iterator.next();
        boolean actual3 = iterator.hasNext();
        iterator.next();
        boolean actual4 = iterator.hasNext();
        iterator.next();
        boolean actual5 = iterator.hasNext();
        iterator.next();
        boolean actual6 = iterator.hasNext();
        //THEN
        Assert.assertTrue(actual0);
        Assert.assertTrue(actual1);
        Assert.assertTrue(actual2);
        Assert.assertTrue(actual3);
        Assert.assertTrue(actual4);
        Assert.assertTrue(actual5);
        Assert.assertFalse(actual6);
    }

    @Test
    public void next() {
        //GIVEN
        String expectedResult1 = "111";
        String expectedResult4 = "444";
        String expectedResult3 = "33333";
        String expectedResult0 = "Initial";
        String expectedResult2 = "Changed";
        String expectedResult5 = "Once more";
        //WHEN
        myHashMap.put(new My(expectedResult0), "Initial");
        myHashMap.put(new My(expectedResult1), "111");
        myHashMap.put(new My(expectedResult2), "Changed");
        myHashMap.put(new My(expectedResult3), "33333");
        myHashMap.put(new My(expectedResult4), "444");
        myHashMap.put(new My(expectedResult5), "Once more");
        Iterator<Map.Entry<My, String>> iterator = myHashMap.iterator();
        String actualResult1 = iterator.next().value;
        String actualResult4 = iterator.next().value;
        String actualResult3 = iterator.next().value;
        String actualResult0 = iterator.next().value;
        String actualResult2 = iterator.next().value;
        String actualResult5 = iterator.next().value;
        //THEN
        Assert.assertNotNull(actualResult0);
        Assert.assertNotNull(actualResult1);
        Assert.assertNotNull(actualResult2);
        Assert.assertNotNull(actualResult3);
        Assert.assertNotNull(actualResult4);
        Assert.assertNotNull(actualResult5);
        Assert.assertEquals(expectedResult0, actualResult0);
        Assert.assertEquals(expectedResult1, actualResult1);
        Assert.assertEquals(expectedResult2, actualResult2);
        Assert.assertEquals(expectedResult3, actualResult3);
        Assert.assertEquals(expectedResult4, actualResult4);
        Assert.assertEquals(expectedResult5, actualResult5);
    }

    @Test
    public void remove() {
        //GIVEN
        String expectedResult0 = "444";
        String expectedResult1 = "Initial";
        //WHEN
        myHashMap.put(new My("Initial"), "Initial");
        myHashMap.put(new My("111"), "111");
        myHashMap.put(new My("Changed"), "Changed");
        myHashMap.put(new My("33333"), "33333");
        myHashMap.put(new My("444"), "444");
        myHashMap.put(new My("Once more"), "Once more");
        Iterator<Map.Entry<My, String>> iterator = myHashMap.iterator();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.remove();
        iterator = myHashMap.iterator();
        String actualResult0 = iterator.next().value;
        String actualResult1 = iterator.next().value;
        //THEN
        Assert.assertNotNull(actualResult0);
        Assert.assertNotNull(actualResult1);
        Assert.assertEquals(expectedResult0, actualResult0);
        Assert.assertEquals(expectedResult1, actualResult1);
    }
}