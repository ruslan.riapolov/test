package com;

import com.collection.impl.MyTreeMap;
import com.epm.lab.collections.Map;
import com.epm.lab.collections.OrderedMap;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;

/**
 * MyTreeMapTest
 *
 * @author Ruslan Riapolov
 * @version 1.0 8/5/2018 10:25 AM
 */
public class MyTreeMapTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private OrderedMap<Integer, String> myTreeMap = new MyTreeMap<>();

    @Test
    public void get() {
        //GIVEN
        String expectedResult = "789";
        //WHEN
        myTreeMap.put(123, expectedResult);
        String actualNullResult = myTreeMap.get(456);
        String actualResult = myTreeMap.get(123);
        //THEN
        Assert.assertNull(actualNullResult);
        Assert.assertNotNull(actualResult);
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void putNullKey() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Key cannot be null!!!");
        myTreeMap.put(null, "NULL key");
    }

    @Test
    public void putNullValue() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Value cannot be null!!!");
        myTreeMap.put(123, null);
    }

    @Test
    public void put() {
        //GIVEN
        String expectedResult0 = "111";
        String expectedResult1 = "22";
        String expectedResult2 = "333";
        String expectedResult3 = "4444";
        String expectedResult4 = "55555";
        String expectedResult5 = "666666";
        //WHEN
        myTreeMap.put(4444, expectedResult3);
        myTreeMap.put(111, expectedResult0);
        myTreeMap.put(666666, expectedResult5);
        myTreeMap.put(22, expectedResult1);
        myTreeMap.put(333, "something before replace");
        myTreeMap.put(55555, expectedResult4);
        myTreeMap.put(333, expectedResult2);
        myTreeMap.put(0, "000000000");
        myTreeMap.put(999999999, "999999999");
        myTreeMap.put(111111111, "111111111");
        myTreeMap.put(888888888, "888888888");
        myTreeMap.put(222222222, "222222222");
        myTreeMap.put(777777777, "777777777");
        myTreeMap.put(333333333, "333333333");
        myTreeMap.put(666666666, "666666666");
        myTreeMap.put(444444444, "444444444");
        myTreeMap.put(555555555, "555555555");
        String actualNullResult = myTreeMap.get(1234567890);
        String actualResult0 = myTreeMap.get(111);
        String actualResult1 = myTreeMap.get(22);
        String actualResult2 = myTreeMap.get(333);
        String actualResult3 = myTreeMap.get(4444);
        String actualResult4 = myTreeMap.get(55555);
        String actualResult5 = myTreeMap.get(666666);
        //THEN
        Assert.assertNull(actualNullResult);
        Assert.assertNotNull(actualResult0);
        Assert.assertNotNull(actualResult1);
        Assert.assertNotNull(actualResult2);
        Assert.assertNotNull(actualResult3);
        Assert.assertNotNull(actualResult4);
        Assert.assertNotNull(actualResult5);
        Assert.assertEquals(expectedResult0, actualResult0);
        Assert.assertEquals(expectedResult1, actualResult1);
        Assert.assertEquals(expectedResult2, actualResult2);
        Assert.assertEquals(expectedResult3, actualResult3);
        Assert.assertEquals(expectedResult4, actualResult4);
        Assert.assertEquals(expectedResult5, actualResult5);
    }

    @Test
    public void size() {
        //GIVEN
        int expectedSize0 = 0;
        int expectedSize1 = 6;
        int expectedSize2 = 2;
        int expectedSize3 = 17;
        //WHEN
        int actualSize0 = myTreeMap.size();
        myTreeMap.put(4, "5");
        myTreeMap.put(1, "1");
        myTreeMap.put(6, "6");
        myTreeMap.put(2, "2");
        myTreeMap.put(3, "3");
        myTreeMap.put(5, "5");
        int actualSize1 = myTreeMap.size();
        Iterator<Map.Entry<Integer, String>> iterator = myTreeMap.iterator();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.remove();
        int actualSize2 = myTreeMap.size();
        myTreeMap.put(7, "7");
        myTreeMap.put(22, "22");
        myTreeMap.put(21, "21");
        myTreeMap.put(8, "8");
        myTreeMap.put(9, "9");
        myTreeMap.put(10, "10");
        myTreeMap.put(20, "20");
        myTreeMap.put(19, "19");
        myTreeMap.put(18, "18");
        myTreeMap.put(11, "11");
        myTreeMap.put(12, "12");
        myTreeMap.put(17, "17");
        myTreeMap.put(16, "16");
        myTreeMap.put(13, "13");
        myTreeMap.put(14, "14");
        myTreeMap.put(15, "15");
        iterator = myTreeMap.iterator();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.remove();
        int actualSize3 = myTreeMap.size();
        //THEN
        Assert.assertEquals(expectedSize0, actualSize0);
        Assert.assertEquals(expectedSize1, actualSize1);
        Assert.assertEquals(expectedSize2, actualSize2);
        Assert.assertEquals(expectedSize3, actualSize3);
    }

    @Test
    public void hasNext() {
        //GIVEN
        //WHEN
        myTreeMap.put(4444, "4444");
        myTreeMap.put(111, "111");
        myTreeMap.put(666666, "666666");
        myTreeMap.put(22, "22");
        myTreeMap.put(333, "333");
        myTreeMap.put(55555, "55555");
        Iterator<Map.Entry<Integer, String>> iterator = myTreeMap.iterator();
        boolean actual0 = iterator.hasNext();
        iterator.next();
        boolean actual1 = iterator.hasNext();
        iterator.next();
        boolean actual2 = iterator.hasNext();
        iterator.next();
        boolean actual3 = iterator.hasNext();
        iterator.next();
        boolean actual4 = iterator.hasNext();
        iterator.next();
        boolean actual5 = iterator.hasNext();
        iterator.next();
        boolean actual6 = iterator.hasNext();
        //THEN
        Assert.assertTrue(actual0);
        Assert.assertTrue(actual1);
        Assert.assertTrue(actual2);
        Assert.assertTrue(actual3);
        Assert.assertTrue(actual4);
        Assert.assertTrue(actual5);
        Assert.assertFalse(actual6);
    }

    @Test
    public void next() {
        //GIVEN
        String expectedResult0 = "22";
        String expectedResult1 = "111";
        String expectedResult2 = "333";
        String expectedResult3 = "4444";
        String expectedResult4 = "55555";
        String expectedResult5 = "666666";
        //WHEN
        myTreeMap.put(4444, "4444");
        myTreeMap.put(111, "111");
        myTreeMap.put(666666, "666666");
        myTreeMap.put(22, "22");
        myTreeMap.put(333, "333");
        myTreeMap.put(55555, "55555");
        Iterator<Map.Entry<Integer, String>> iterator = myTreeMap.iterator();
        String actualResult0 = iterator.next().value;
        String actualResult1 = iterator.next().value;
        String actualResult2 = iterator.next().value;
        String actualResult3 = iterator.next().value;
        String actualResult4 = iterator.next().value;
        String actualResult5 = iterator.next().value;
        //THEN
        Assert.assertNotNull(actualResult0);
        Assert.assertNotNull(actualResult1);
        Assert.assertNotNull(actualResult2);
        Assert.assertNotNull(actualResult3);
        Assert.assertNotNull(actualResult4);
        Assert.assertNotNull(actualResult5);
        Assert.assertEquals(expectedResult0, actualResult0);
        Assert.assertEquals(expectedResult1, actualResult1);
        Assert.assertEquals(expectedResult2, actualResult2);
        Assert.assertEquals(expectedResult3, actualResult3);
        Assert.assertEquals(expectedResult4, actualResult4);
        Assert.assertEquals(expectedResult5, actualResult5);
    }

    @Test
    public void remove() {
        //GIVEN
        String expectedResult0 = "111";
        String expectedResult1 = "4444";
        //WHEN
        myTreeMap.put(4444, "4444");
        myTreeMap.put(111, "111");
        myTreeMap.put(666666, "666666");
        myTreeMap.put(22, "22");
        myTreeMap.put(333, "333");
        myTreeMap.put(55555, "55555");
        Iterator<Map.Entry<Integer, String>> iterator = myTreeMap.iterator();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.next();
        iterator.remove();
        iterator.remove();
        iterator = myTreeMap.iterator();
        String actualResult0 = iterator.next().value;
        String actualResult1 = iterator.next().value;
        //THEN
        Assert.assertNotNull(actualResult0);
        Assert.assertNotNull(actualResult1);
        Assert.assertEquals(expectedResult0, actualResult0);
        Assert.assertEquals(expectedResult1, actualResult1);
    }
}