package com;

import com.collection.impl.MyHashMap;
import com.collection.impl.MyTreeMap;
import com.epm.lab.collections.Map;
import com.epm.lab.collections.OrderedMap;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main
 *
 * @author Ruslan Riapolov
 * @version 1.0 7/19/2018 12:26 AM
 */
public class Main {

    private static final Pattern pattern = Pattern.compile("Key: (.*?) Value: (.*)");

    public static void main(String[] args) {
        try {
            System.out.println("This is Demo program for MyHashMap and MyTreeMap." +
                    "\nMyHashMap is used for demonstration of Cache and MyTreeMap for Sorting." +
                    "\nPlease start it with -h flag if you want to use MyHashMap and with -t in case of MyTreeMap." +
                    "\nYou should use -ih flag for setting input file of MyHashMap." +
                    "\nYou should use -it flag for setting input file of MyTreeMap." +
                    "\n\n-----!!!!!!!!!! THE FILE SAMPLES ARE IN THE ROOT OF ci-1.0-SNAPSHOT-jar-with-dependencies.jar !!!!!!!!!!-----");

            ResourceBundle properties = ResourceBundle.getBundle(("default"));
            boolean useHashMap = false;
            boolean useTreeMap = false;
            String inputHash = properties.getString("hash_map_data");
            String inputTree = properties.getString("tree_map_data");
            if (args.length == 0) {
                useHashMap = useTreeMap = true;
            } else {
                for (int i = 0; i < args.length; i++) {
                    String arg = args[i];
                    if ("-h".equals(arg)) {
                        useHashMap = true;
                    } else if ("-ih".equals(arg)) {
                        if (i < args.length - 1 && !args[i + 1].startsWith("-")) {
                            inputHash = args[i + 1];
                        }
                    } else if ("-t".equals(arg)) {
                        useTreeMap = true;
                    } else if ("-it".equals(arg)) {
                        if (i < args.length - 1 && !args[i + 1].startsWith("-")) {
                            inputTree = args[i + 1];
                        }
                    }
                }
            }

            if (useHashMap) {
                if (!Files.exists(Paths.get(inputHash))) {
                    throw new RuntimeException("Data file for MyHashMap is absent!!!");
                }
            }

            if (useTreeMap) {
                if (!Files.exists(Paths.get(inputTree))) {
                    throw new RuntimeException("Data file for MyTreeMap is absent!!!");
                }
            }

            if (useHashMap) {
                System.out.println("\nMyHashMap (Cache)\n------------------------------\n");
                final Map<String, String> cache = new MyHashMap<>();
                Files.lines(Paths.get(inputHash)).forEach(string -> {
                    try {
                        Matcher matcher = pattern.matcher(string);
                        if (!matcher.find()) {
                            throw new RuntimeException("Parsing exception");
                        }
                        String key = matcher.group(1).trim();
                        String value = cache.get(key);
                        if (value != null) {
                            value = "(From Cache) ".concat(value);
                        } else {
                            value = matcher.group(2).trim();
                            cache.put(key, value);
                        }
                        System.out.println("Key: " + key + " Value: " + value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }

            if (useTreeMap) {
                System.out.println("\nMyTreeMap (Sorting)\n------------------------------\n");
                final OrderedMap<Integer, String> sorting = new MyTreeMap<>();
                Files.lines(Paths.get(inputTree)).forEach(string -> {
                    try {
                        Matcher matcher = pattern.matcher(string);
                        if (!matcher.find()) {
                            throw new RuntimeException("Parsing exception");
                        }
                        Integer key = 0;
                        try {
                            key = Integer.parseInt(matcher.group(1).trim());
                        } catch (NumberFormatException ex) {
                            ex.printStackTrace();
                        }
                        String value = matcher.group(2).trim();
                        sorting.put(key, value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                sorting.forEach(entry -> System.out.println("Key: " + entry.key + " Value: " + entry.value));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
