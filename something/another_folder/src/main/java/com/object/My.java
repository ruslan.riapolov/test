package com.object;

/**
 * My
 *
 * @author Ruslan Riapolov
 * @version 1.0 8/8/2018 7:16 PM
 */
public class My {
    private String data;

    public My(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    @Override
    public boolean equals(Object obj) {
        return data.equals(((My) obj).getData());
    }

    @Override
    public int hashCode() {
        return data.length() == 5 ? 5 : (data.length() > 5 ? 7 : 3);
    }

    @Override
    public String toString() {
        return data;
    }
}
