package com.collection.impl;


import com.epm.lab.collections.OrderedMap;

import java.util.Iterator;

/**
 * MyTreeMap
 *
 * @author Ruslan Riapolov
 * @version 1.0 8/8/2018 2:07 PM
 */
public class MyTreeMap<K extends Comparable<K>, V> implements OrderedMap<K, V> {
    private Item root;
    private int size;

    @Override
    public V get(K key) {
        Item current = root;
        while (current != null) {
            int compareTo = -current.data.key.compareTo(key);
            if (compareTo == 0) {
                return current.data.value;
            } else {
                current = compareTo < 0 ? current.left : current.right;
            }
        }
        return null;
    }

    @Override
    public void put(K key, V value) {
        if (key == null) {
            throw new RuntimeException("Key cannot be null!!!");
        } else if (value == null) {
            throw new RuntimeException("Value cannot be null!!!");
        }

        Item newItem = new Item(new Entry<>(key, value));
        if (root == null) {
            root = newItem;
            size++;
            return;
        }
        Item current = root;
        while (true) {
            int compareTo = key.compareTo(current.data.key);
            if (compareTo == 0) {
                current.data.value = value;
                return;
            } else if (compareTo < 0) {
                if (current.left == null) {
                    current.left = newItem;
                    size++;
                    return;
                }
                current = current.left;
            } else {
                if (current.right == null) {
                    current.right = newItem;
                    size++;
                    return;
                }
                current = current.right;
            }
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {
            private Stack stack;
            private Item currentItem;

            {
                if (stack == null) {
                    stack = new Stack();
                    Item item = root;
                    while (item != null) {
                        stack.push(item);
                        item = item.left;
                    }
                }
            }

            @Override
            public boolean hasNext() {
                return !stack.isEmpty();
            }

            @Override
            public Entry<K, V> next() {
                Entry<K, V> result = null;
                Item item = stack.pop();
                currentItem = item;
                if (item != null) {
                    result = item.data;
                    if (item.right != null) {
                        item = item.right;
                        while (item != null) {
                            stack.push(item);
                            item = item.left;
                        }
                    }
                }
                return result;
            }

            @Override
            public void remove() {
                if (currentItem != null) {
                    if (currentItem.left != null && currentItem.right != null) {
                        Item replacerParent = currentItem;
                        Item replacer = currentItem.right;
                        while (replacer.left != null) {
                            replacerParent = replacer;
                            replacer = replacer.left;
                        }
                        if (replacerParent.left == replacer) {
                            replacerParent.left = replacer.right;
                        } else {
                            replacerParent.right = replacer.right;
                        }


                        if (currentItem != root) {
                            Item parent = parent(currentItem);
                            if (parent.left == currentItem) {
                                parent.left = replacer;
                            } else {
                                parent.right = replacer;
                            }
                        } else {
                            root = replacer;
                        }

                        replacer.left = currentItem.left;
                        replacer.right = currentItem.right;

                        currentItem = null;
                        stack = new Stack();
                        Item item = root;
                        while (item != null) {
                            stack.push(item);
                            item = item.left;
                        }
                        do {
                            next();
                        } while (currentItem != null && currentItem != replacer);
                    } else {
                        Item removingItem = currentItem;
                        next();
                        if (removingItem != root) {
                            Item parent = parent(removingItem);
                            if (parent.left == removingItem) {
                                parent.left = removingItem.left != null ? removingItem.left : removingItem.right;
                            } else {
                                parent.right = removingItem.left != null ? removingItem.left : removingItem.right;
                            }
                        } else {
                            root = removingItem.left != null ? removingItem.left : removingItem.right;
                        }
                    }
                    size--;
                }
            }

            private Item parent(Item child) {
                Item parent = null;
                Stack tempStack = new Stack();
                Item item = root;
                while (item != null) {
                    tempStack.push(item);
                    item = item.left;
                }
                while (!tempStack.isEmpty()) {
                    item = tempStack.pop();
                    if (item.left == child || item.right == child) {
                        parent = item;
                        break;
                    }
                    if (item.right != null) {
                        item = item.right;
                        while (item != null) {
                            tempStack.push(item);
                            item = item.left;
                        }
                    }
                }
                return parent;
            }

            class Stack {
                private StackItem top;

                void push(Item item) {
                    top = new StackItem(item, top);
                }

                Item pop() {
                    Item returnValue = null;
                    if (top != null) {
                        StackItem topValue = top;
                        top = topValue.getLink();
                        returnValue = topValue.getData();
                    }
                    return returnValue;
                }

                boolean isEmpty() {
                    return top == null;
                }

                class StackItem {
                    private Item data;
                    private StackItem link;

                    StackItem(Item data, StackItem link) {
                        this.data = data;
                        this.link = link;
                    }

                    Item getData() {
                        Item returnValue = data;
                        data = null;
                        return returnValue;
                    }

                    StackItem getLink() {
                        StackItem returnValue = link;
                        link = null;
                        return returnValue;
                    }
                }
            }
        };
    }

    private class Item {
        private Item left;
        private Item right;
        private Entry<K, V> data;

        Item(Entry<K, V> data) {
            this.data = data;
        }
    }
}
