package com.collection.impl;


import com.epm.lab.collections.Map;

import java.util.Iterator;

/**
 * MyHashMap
 *
 * @author Ruslan Riapolov
 * @version 1.0 8/8/2018 2:07 PM
 */

public class MyHashMap<K, V> implements Map<K, V> {
    private static final int INITIAL_CAPACITY = 16;
    private static final double LOAD_FACTOR = 0.75;
    private int capacity = INITIAL_CAPACITY;
    private Item[] entries = new Item[capacity];
    private int size;

    private void checkCapacity() {
        if (size == capacity * LOAD_FACTOR) {
            Item[] tempEntries = new Item[capacity * 2];
            for (Entry<K, V> entry : this) {
                Item newItem = new Item(entry);
                int bucketNumber = Math.abs(entry.key.hashCode()) % (capacity * 2);
                Item item = tempEntries[bucketNumber];
                if (item == null) {
                    tempEntries[bucketNumber] = newItem;
                } else {
                    while (item.getNext() != null) {
                        item = item.getNext();
                    }
                    item.setNext(newItem);
                }
            }
            entries = tempEntries;
            capacity = capacity * 2;
        }
    }

    private int bucketNumber(K key) {
        return key != null ? Math.abs(key.hashCode()) % capacity : 0;
    }

    private Item getItem(K key) {
        Item item = entries[bucketNumber(key)];
        while (item != null) {
            if (key == null && item.getData().key == null
                    || key != null && key.equals(item.getData().key)) {
                break;
            }
            item = item.getNext();
        }
        return item;
    }

    @Override
    public V get(K key) {
        Item item = getItem(key);
        return item != null ? (V) item.getData().value : null;
    }

    @Override
    public void put(K key, V value) {
        if (key == null) {
            throw new RuntimeException("Key cannot be null!!!");
        } else if (value == null) {
            throw new RuntimeException("Value cannot be null!!!");
        }
        Item item = getItem(key);
        if (item != null) {
            item.getData().value = value;
        } else {
            checkCapacity();
            Item newItem = new Item(new Entry<>(key, value));
            int bucketNumber = bucketNumber(key);
            item = entries[bucketNumber];
            if (item == null) {
                entries[bucketNumber] = newItem;
            } else {
                while (item.getNext() != null) {
                    item = item.getNext();
                }
                item.setNext(newItem);
            }
            size++;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {
            private int currentIndex = -1;
            private Item currentItem;
            private ItemWithIndex next;

            private ItemWithIndex findNext() {
                int index = currentIndex;
                Item item = currentItem;
                if (item != null) {
                    item = item.getNext();
                }
                while (item == null) {
                    if (index == capacity - 1) {
                        break;
                    }
                    item = entries[++index];
                }
                return new ItemWithIndex(index, item);
            }

            @Override
            public boolean hasNext() {
                next = findNext();
                return next.getItem() != null;
            }

            @Override
            public Entry<K, V> next() {
                if (next == null) {
                    next = findNext();
                }
                currentIndex = next.getIndex();
                currentItem = next.getItem();
                next = null;
                return currentItem != null ? currentItem.getData() : null;
            }

            @Override
            public void remove() {
                if (currentItem != null) {
                    Item previous = null;
                    Item item = entries[currentIndex];
                    while (!currentItem.equals(item)) {
                        previous = item;
                        item = item.getNext();
                    }
                    if (previous == null) {
                        entries[currentIndex] = item.getNext();
                    } else {
                        previous.setNext(item.getNext());
                    }
                    next();
                    size--;
                }
            }

            class ItemWithIndex {
                private int index;
                private Item item;

                ItemWithIndex(int index, Item item) {
                    this.index = index;
                    this.item = item;
                }

                int getIndex() {
                    return index;
                }

                Item getItem() {
                    return item;
                }
            }
        };
    }

    private class Item<K, V> {
        private Item next;
        private Entry<K, V> data;

        Item(Entry<K, V> data) {
            this.data = data;
        }

        Item getNext() {
            return next;
        }

        void setNext(Item next) {
            this.next = next;
        }

        Entry<K, V> getData() {
            return data;
        }
    }
}
